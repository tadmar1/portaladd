package com.tadmar.portal.PortalAdd.data.shared;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.tadmar.portal.PortalAdd.model.MobileMenu;
import com.tadmar.portal.PortalAdd.model.shared.CusrDa;
import com.tadmar.portal.PortalAdd.model.shared.JobPos;
import com.tadmar.utils.sql.SqlExp;
import com.tadmar.utils.sql.SqlExpAnotations.SqlExpKey;

@Stateless
public class EnvirRepo {

	@Inject @SqlExpKey(key="AD")
	SqlExp sqlExpressionService;
	@SuppressWarnings("unused")
	@Inject private transient Logger logger;

	@PersistenceContext(unitName = "SG_DANEORGANIZACJA")
    private EntityManager emSp;

	@PersistenceContext(unitName = "S65BCC4B")
    private EntityManager emMvx;

	public CusrDa findById( String cusrid) {
    	@SuppressWarnings("unchecked")
		List<CusrDa> cusrdaList =   emSp.createNativeQuery(sqlExpressionService.getSqlExpressionEntry("EnvirRepo.CusrDaFindById")
    			.replace("{0}", cusrid)
				,CusrDa.class).getResultList();
    	return cusrdaList.isEmpty() ? new CusrDa() : cusrdaList.get(0);
	}

	public CusrDa findByRole( String curoid) {
    	@SuppressWarnings("unchecked")
		List<CusrDa> cusrdaList =   emSp.createNativeQuery(sqlExpressionService.getSqlExpressionEntry("EnvirRepo.CusrDaFindByRole")
    			.replace("{0}", curoid)
				,CusrDa.class).getResultList();
    	return cusrdaList.isEmpty() ? new CusrDa() : cusrdaList.get(0);
	}

	public CusrDa findByAdditionalRole( String curoid) {
    	@SuppressWarnings("unchecked")
		List<CusrDa> cusrdaList =   emSp.createNativeQuery(sqlExpressionService.getSqlExpressionEntry("EnvirRepo.CusrDaFindByAdditionalRole")
    			.replace("{0}", curoid)
				,CusrDa.class).getResultList();
    	return cusrdaList.isEmpty() ? new CusrDa() : cusrdaList.get(0);
	}

	@SuppressWarnings("unchecked")
	public List<JobPos> findJobsPositions() {
		return emSp.createNativeQuery(sqlExpressionService.getSqlExpressionEntry("EnvirRepo.findJobsPositions")
				,JobPos.class).getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<MobileMenu> findMenuForMobile(String currType) {
    	return  emSp.createNativeQuery(sqlExpressionService.getSqlExpressionEntry("EnvirRepo.findMenuForMobile")
    			.replace("{0}", currType)
				,MobileMenu.class).getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<MobileMenu> findMenuForDesktop(String currType) {
    	return  emSp.createNativeQuery(sqlExpressionService.getSqlExpressionEntry("EnvirRepo.findMenuForDesktop")
    			.replace("{0}", currType)
				,MobileMenu.class).getResultList();
	}

	public Integer findTasksNo(String sqlText) {
		return (int) emSp.createNativeQuery(sqlText.trim()).getSingleResult();
 	}

}
