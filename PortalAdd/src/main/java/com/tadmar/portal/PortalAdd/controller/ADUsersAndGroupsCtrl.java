package com.tadmar.portal.PortalAdd.controller;

import java.io.Serializable;
import java.util.Base64;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.naming.NamingException;
import javax.naming.ldap.LdapContext;

import org.apache.commons.io.FilenameUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.file.UploadedFile;

import com.tadmar.portal.PortalAdd.data.shared.EnvirRepo;
import com.tadmar.portal.PortalAdd.model.ADInfo;
import com.tadmar.portal.PortalAdd.model.ADInfoExt;
import com.tadmar.portal.PortalAdd.model.shared.CusrDa;
import com.tadmar.portal.PortalAdd.model.shared.JobPos;
import com.tadmar.portal.PortalAdd.service.ADPersonGlobalServ;
import com.tadmar.portal.PortalAdd.service.ADSearch;
import com.tadmar.portal.PortalAdd.util.Utils;
import com.tadmar.utils.faces.FacesUtils;


@Named(value="ADUsersAndGroupsCtrl")
@ViewScoped
public class ADUsersAndGroupsCtrl implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private CusrDa cusrda;
	@Inject private EnvirRepo envirRepo;
	@Inject private ADSearch adsearchServ;
	private ADInfoExt adinfoExt;
	private ADInfo adinfo;
	private List<ADInfo> adinfoPersonGlobalList;
	private ADInfo adinfoPersonGlobalSelected;
	private List<ADInfo> adinfoPersonOfDepartemntList;
	@Inject private FacesUtils facesUtils;
	@Inject private ADPersonGlobalServ adPersonGlobalServ;
	private String departmentName;
	private Integer currUserAndGrouptv;
	private String groupAddInfo;
	private List<JobPos> jobPosList;
	private JobPos jobPosDescription;
	private JobPos jobPosTitle;
	private ADInfo jobPosManager;
	
	/********************************************************************************************************
	 *  Constructors
	 ********************************************************************************************************/
	@PostConstruct
	private void init() {
		adinfo = new ADInfo();
		adinfoPersonGlobalSelected = new ADInfo();
		cusrda = envirRepo.findById(Utils.getUserName());
		adinfoPersonGlobalList = adPersonGlobalServ.getAdinfoList();
		adinfoPersonGlobalSelected = findAdinfoPersonGlobalSelected(Utils.getUserName(), adinfoPersonGlobalList);
		jobPosList = adPersonGlobalServ.getJobPosList();
		Map<String, String> requestMap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
    	groupAddInfo = "";
    	if (requestMap.get("department") != null) {
    		currUserAndGrouptv = 1;
    		//envirRepo.findByRole(requestMap.get("department").toUpperCase().trim()).getCusrid();
    		adinfoPersonGlobalSelected = findAdinfoPersonGlobalSelected(
    				envirRepo.findByRole(requestMap.get("department").toUpperCase().trim()).getCusrid(), 
    				adinfoPersonGlobalList);
    	} else {
    		currUserAndGrouptv = 0;
    		adinfoPersonGlobalSelected = findAdinfoPersonGlobalSelected(Utils.getUserName(), adinfoPersonGlobalList);
    	}
    	if (requestMap.get("info") != null) {
    		groupAddInfo = requestMap.get("info") ;
    	} else {
        	groupAddInfo = "";
    	}

    	onRetrieveUserInfo();
	}

	/********************************************************************************************************
	 *  Events
	 ********************************************************************************************************/
	public void onRetrieveUserInfo() {
		LdapContext ctx = adPersonGlobalServ.getLdapContext();
		adinfo = adsearchServ.getUserBasicAttributes(adinfoPersonGlobalSelected.sAMAccountName, ctx, adsearchServ.getUserAttrIDs(),"");
		adinfoPersonOfDepartemntList = adsearchServ.getPersonOfDepartment(adinfo, ctx, adsearchServ.getUserAttrIDs()); 
		departmentName = adsearchServ.getDepartmentName(adinfo);
		try {
			ctx.close();
		} catch (NamingException e) {
			e.printStackTrace();
		}
		jobPosManager = findJobPosManagerFromList(adinfoPersonGlobalList, adinfo.getManager());
		jobPosDescription = findJobPositionFromList(jobPosList, adinfo.getDescription());
		jobPosTitle = findJobPositionFromList(jobPosList, adinfo.getTitle());
	}

	public void onUserSelected() {
		onRetrieveUserInfo();
	}

	public void onPhotoUpload(FileUploadEvent event) {
		if (!adinfoPersonGlobalSelected.sAMAccountName.toUpperCase().trim().equals(Utils.getUserName().toUpperCase().trim())) {
			Utils.dspMsg(FacesMessage.SEVERITY_ERROR, "Można zmieniać tylko swoje dane");
			return;
		}
		String uploadedFile = fileUploadHandler(event, adinfo );
		if (uploadedFile.length() > 0 ) {
			facesUtils.updateUiWidget("idUserPanel");
			adsearchServ.ModifyThumbnailPhoto(adinfo.getDistinguishedName(), adinfo.getThumbnailPhoto());
			Utils.dspMsg(FacesMessage.SEVERITY_INFO, "Plik zapisany: " + uploadedFile);
		}
		
	}

	public void onInfoSave() {
		if (!adinfoPersonGlobalSelected.sAMAccountName.toUpperCase().trim().equals(Utils.getUserName().toUpperCase().trim())) {
			Utils.dspMsg(FacesMessage.SEVERITY_ERROR, "Można zmieniać tylko swoje dane");
			return;
		}
		adsearchServ.ModifyInfo(adinfo.getDistinguishedName(), adinfo);
		Utils.dspMsg(FacesMessage.SEVERITY_INFO, "Dane zostały zapisane: ");
	}
	
	public void onUserDescriptionSelected() {
		adinfo.setDescription(jobPosDescription.getPosname().trim());
	}
	
	public void onUserTitleSelected() {
		adinfo.setTitle(jobPosTitle.getPosname().trim());
	}
	
	public void onUserManagerSelected() {
		
		LdapContext ctx = adPersonGlobalServ.getLdapContext();
		jobPosManager = adsearchServ.getUserBasicAttributes(jobPosManager.sAMAccountName, ctx, adsearchServ.getUserAttrIDs(),"");
		
		adinfo.setManager(jobPosManager.cn);
		adinfo.setNotRetrievedManager(jobPosManager.distinguishedName);
		System.out.println(jobPosManager);

	}

	/********************************************************************************************************
	 *  Methods
	 ********************************************************************************************************/

	private ADInfo findAdinfoPersonGlobalSelected(String currRetrvUser, List<ADInfo> adinfoPersonGlobalList) {
		
		ADInfo adinfo = new ADInfo();
		for(ADInfo adinfol :adinfoPersonGlobalList) {
			if(adinfol.sAMAccountName.equalsIgnoreCase(currRetrvUser)) {
				adinfo = adinfol;
			}
		}
		return adinfo;
	}

	public String  fileUploadHandler(FileUploadEvent event, ADInfo adinfo ) {
		
		String returnMessage= "";
		try {
			UploadedFile file = event.getFile();
			String fileName = file.getFileName();
				adinfo.setThumbnailPhoto(file.getContent());
				adinfo.setNotRetrieved("data:image/" + FilenameUtils.getExtension(fileName).trim() + ";base64," 
						+ Base64.getEncoder().encodeToString(file.getContent()));
			returnMessage = fileName + " - rozmiar :" + event.getFile().getSize() + "B pobrany";
    	} catch(Exception ex) {}
		return returnMessage;
	}
	
	private JobPos findJobPositionFromList(List<JobPos> jobPosList, String description) {
		JobPos jobPos = new JobPos();
		
		for ( JobPos jobPosIt : jobPosList) {
			if (description != null && jobPosIt.getPosname().trim().equals(description.trim())) {
				jobPos = jobPosIt;
			}
		}
		return jobPos;
	}
	
	private ADInfo findJobPosManagerFromList(List<ADInfo> adinfoPersonGlobalList, String manager) {
		
		ADInfo adinfo = new ADInfo();
		
		for ( ADInfo adinfoIt : adinfoPersonGlobalList ) {
			if (adinfoIt.getCn() != null && manager != null) {
				if (adinfoIt.getCn().trim().equals(manager.trim())) {
					adinfo = adinfoIt;
				}
			}
		}
		
		return adinfo;
	}
	/********************************************************************************************************
	 *  Get-Set
	 ********************************************************************************************************/
	public CusrDa getCusrda() {
		return cusrda;
	}
	public void setCusrda(CusrDa cusrda) {
		this.cusrda = cusrda;
	}
	public EnvirRepo getEnvirRepo() {
		return envirRepo;
	}
	public void setEnvirRepo(EnvirRepo envirRepo) {
		this.envirRepo = envirRepo;
	}
	public ADInfoExt getadinfoExt() {
		return adinfoExt;
	}
	public void setadinfoExt(ADInfoExt adinfoExt) {
		this.adinfoExt = adinfoExt;
	}
	public ADInfo getAdinfo() {
		return adinfo;
	}
	public void setAdinfo(ADInfo adinfo) {
		this.adinfo = adinfo;
	}
	public List<ADInfo> getAdinfoPersonGlobalList() {
		return adinfoPersonGlobalList;
	}
	public void setAdinfoPersonGlobalList(List<ADInfo> adinfoPersonGlobalList) {
		this.adinfoPersonGlobalList = adinfoPersonGlobalList;
	}
	public ADInfo getAdinfoPersonGlobalSelected() {
		return adinfoPersonGlobalSelected;
	}
	public void setAdinfoPersonGlobalSelected(ADInfo adinfoPersonGlobalSelected) {
		this.adinfoPersonGlobalSelected = adinfoPersonGlobalSelected;
	}

	public List<ADInfo> getAdinfoPersonOfDepartemntList() {
		return adinfoPersonOfDepartemntList;
	}

	public void setAdinfoPersonOfDepartemntList(
			List<ADInfo> adinfoPersonOfDepartemntList) {
		this.adinfoPersonOfDepartemntList = adinfoPersonOfDepartemntList;
	}

	public Integer getCurrUserAndGrouptv() {
		return currUserAndGrouptv;
	}

	public void setCurrUserAndGrouptv(Integer currUserAndGrouptv) {
		this.currUserAndGrouptv = currUserAndGrouptv;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public String getGroupAddInfo() {
		return groupAddInfo;
	}

	public void setGroupAddInfo(String groupAddInfo) {
		this.groupAddInfo = groupAddInfo;
	}

	public List<JobPos> getJobPosList() {
		return jobPosList;
	}

	public void setJobPosList(List<JobPos> jobPosList) {
		this.jobPosList = jobPosList;
	}

	public JobPos getJobPosDescription() {
		return jobPosDescription;
	}

	public void setJobPosDescription(JobPos jobPosDescription) {
		this.jobPosDescription = jobPosDescription;
	}

	public JobPos getJobPosTitle() {
		return jobPosTitle;
	}

	public void setJobPosTitle(JobPos jobPosTitle) {
		this.jobPosTitle = jobPosTitle;
	}

	public ADInfo getJobPosManager() {
		return jobPosManager;
	}

	public void setJobPosManager(ADInfo jobPosManager) {
		this.jobPosManager = jobPosManager;
	}
}
