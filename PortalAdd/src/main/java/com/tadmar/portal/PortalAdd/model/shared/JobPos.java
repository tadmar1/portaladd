package com.tadmar.portal.PortalAdd.model.shared;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class JobPos implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	private String posname;
	private String posrole;
	private String posstat;
	
	public String getPosname() {
		return posname;
	}
	public void setPosname(String posname) {
		this.posname = posname;
	}
	public String getPosrole() {
		return posrole;
	}
	public void setPosrole(String posrole) {
		this.posrole = posrole;
	}
	public String getPosstat() {
		return posstat;
	}
	public void setPosstat(String posstat) {
		this.posstat = posstat;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((posname == null) ? 0 : posname.hashCode());
		result = prime * result + ((posrole == null) ? 0 : posrole.hashCode());
		result = prime * result + ((posstat == null) ? 0 : posstat.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof JobPos))
			return false;
		JobPos other = (JobPos) obj;
		if (posname == null) {
			if (other.posname != null)
				return false;
		} else if (!posname.equals(other.posname))
			return false;
		if (posrole == null) {
			if (other.posrole != null)
				return false;
		} else if (!posrole.equals(other.posrole))
			return false;
		if (posstat == null) {
			if (other.posstat != null)
				return false;
		} else if (!posstat.equals(other.posstat))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "JobPos [posname=" + posname + ", posrole=" + posrole
				+ ", posstat=" + posstat + "]";
	}
}
