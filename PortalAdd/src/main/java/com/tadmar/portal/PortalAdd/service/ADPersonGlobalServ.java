package com.tadmar.portal.PortalAdd.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;

import org.omnifaces.cdi.Startup;

import com.tadmar.portal.PortalAdd.data.shared.EnvirRepo;
import com.tadmar.portal.PortalAdd.model.ADInfo;
import com.tadmar.portal.PortalAdd.model.shared.JobPos;

@Startup
@Named
@ApplicationScoped
public class ADPersonGlobalServ implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String LDAP_SECURITY_AUTHENTICATION= "Simple";
	private static final String LDAP_SECURITY_PRINCIPAL = "TDM\\CPLSVC-TADMAR-Mantis";
	private static final String LDAP_SECURITY_CREDENTIALS = "xSpa6QgyKGvpAJ2";
	private static final String LDAP_PROVIDER_URL = "ldap://tdmpsdc001.tdm.local:389";

	private List<ADInfo> adinfoList;
	
	@Inject EnvirRepo envirRepo;
	private List<JobPos> jobPosList;
	
	@PostConstruct
	private void init() {
		LdapContext ctx = getLdapContext();
		adinfoList = getAllUserList(ctx);
		jobPosList = envirRepo.findJobsPositions();
		try {
			ctx.close();
		} catch (NamingException e) {
			e.printStackTrace();
		}
		System.out.println("Global start");
	}

	public LdapContext getLdapContext(){
		LdapContext ctx = null;
		try{
			Hashtable<String, String> env = new Hashtable<String, String>();
			env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
			env.put(Context.SECURITY_AUTHENTICATION, LDAP_SECURITY_AUTHENTICATION);
			env.put(Context.SECURITY_PRINCIPAL, LDAP_SECURITY_PRINCIPAL);
			env.put(Context.SECURITY_CREDENTIALS, LDAP_SECURITY_CREDENTIALS);
			env.put(Context.PROVIDER_URL, LDAP_PROVIDER_URL);
			ctx = new InitialLdapContext(env, null);
		} catch(NamingException nex){
			System.out.println("LDAP Connection: FAILED");
			nex.printStackTrace();
		}
		return ctx;
	}

	public List<ADInfo> getAllUserList(LdapContext ctx) {

		List<ADInfo> adinfoList = new ArrayList<ADInfo>();
		String[] attrIDs = {"sAMAccountName", "cn", "distinguishedName"};
		String searchNameTemplate ="OU=users, OU=accounts, OU=PL@,OU=BU-SGP-TADMAR,OU=CPL, OU=D00-SGTS-USS, DC=tdm, DC=local";
		SearchControls constraints = new SearchControls();
		constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
		constraints.setReturningAttributes(attrIDs);
		constraints.setCountLimit(0);

		for (int i =1 ; i < 300; i++ ) {
			String searchName = searchNameTemplate.replace("@", String.format ("%04d", i) );
			try {
				NamingEnumeration<SearchResult> answer = ctx.search(searchName, "(sn=*)" , constraints);
				while (answer.hasMore()) {
					Attributes attrs = ((SearchResult) answer.next()).getAttributes();
					ADInfo adinfo = new ADInfo();
					adinfo.setsAMAccountName(attrs.get("sAMAccountName").get(0).toString());
					adinfo.setCn(attrs.get("cn").get(0).toString());
					adinfo.setDistinguishedName(attrs.get("distinguishedName").get(0).toString());
					adinfoList.add(adinfo);
				}
			} catch (NamingException e) {
				System.out.println(e.getMessage());
			}
		}
		return adinfoList.stream().sorted((s1, s2) -> s1.cn.compareTo(s2.cn) ).collect(Collectors.toList());
	}

	
	public List<ADInfo> getAdinfoList() {
		return adinfoList;
	}
	public void setAdinfoList(List<ADInfo> adinfoList) {
		this.adinfoList = adinfoList;
	}

	public List<JobPos> getJobPosList() {
		return jobPosList;
	}

	public void setJobPosList(List<JobPos> jobPosList) {
		this.jobPosList = jobPosList;
	}
}
