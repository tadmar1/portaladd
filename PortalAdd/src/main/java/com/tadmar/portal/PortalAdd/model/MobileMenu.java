package com.tadmar.portal.PortalAdd.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class MobileMenu implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer mmwrid;
	private String mmwlbl;
	private String mmwsig;
	private String mmwlnk;
	private String mmwqry;
	private Integer mmtsno;
	private String mmtype;
	private String mmcolor;
	
	public Integer getMmwrid() {
		return mmwrid;
	}
	public void setMmwrid(Integer mmwrid) {
		this.mmwrid = mmwrid;
	}
	public String getMmwlbl() {
		return mmwlbl;
	}
	public void setMmwlbl(String mmwlbl) {
		this.mmwlbl = mmwlbl;
	}
	public String getMmwsig() {
		return mmwsig;
	}
	public void setMmwsig(String mmwsig) {
		this.mmwsig = mmwsig;
	}
	public String getMmwlnk() {
		return mmwlnk;
	}
	public void setMmwlnk(String mmwlnk) {
		this.mmwlnk = mmwlnk;
	}
	public String getMmwqry() {
		return mmwqry;
	}
	public void setMmwqry(String mmwqry) {
		this.mmwqry = mmwqry;
	}
	public Integer getMmtsno() {
		return mmtsno;
	}
	public void setMmtsno(Integer mmtsno) {
		this.mmtsno = mmtsno;
	}
	public String getMmtype() {
		return mmtype;
	}
	public void setMmtype(String mmtype) {
		this.mmtype = mmtype;
	}
	public String getMmcolor() {
		return mmcolor;
	}
	public void setMmcolor(String mmcolor) {
		this.mmcolor = mmcolor;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((mmtsno == null) ? 0 : mmtsno.hashCode());
		result = prime * result + ((mmtype == null) ? 0 : mmtype.hashCode());
		result = prime * result + ((mmwlbl == null) ? 0 : mmwlbl.hashCode());
		result = prime * result + ((mmwlnk == null) ? 0 : mmwlnk.hashCode());
		result = prime * result + ((mmwqry == null) ? 0 : mmwqry.hashCode());
		result = prime * result + ((mmwrid == null) ? 0 : mmwrid.hashCode());
		result = prime * result + ((mmwsig == null) ? 0 : mmwsig.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof MobileMenu))
			return false;
		MobileMenu other = (MobileMenu) obj;
		if (mmtsno == null) {
			if (other.mmtsno != null)
				return false;
		} else if (!mmtsno.equals(other.mmtsno))
			return false;
		if (mmtype == null) {
			if (other.mmtype != null)
				return false;
		} else if (!mmtype.equals(other.mmtype))
			return false;
		if (mmwlbl == null) {
			if (other.mmwlbl != null)
				return false;
		} else if (!mmwlbl.equals(other.mmwlbl))
			return false;
		if (mmwlnk == null) {
			if (other.mmwlnk != null)
				return false;
		} else if (!mmwlnk.equals(other.mmwlnk))
			return false;
		if (mmwqry == null) {
			if (other.mmwqry != null)
				return false;
		} else if (!mmwqry.equals(other.mmwqry))
			return false;
		if (mmwrid == null) {
			if (other.mmwrid != null)
				return false;
		} else if (!mmwrid.equals(other.mmwrid))
			return false;
		if (mmwsig == null) {
			if (other.mmwsig != null)
				return false;
		} else if (!mmwsig.equals(other.mmwsig))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "MobileMenu [mmwrid=" + mmwrid + ", mmwlbl=" + mmwlbl
				+ ", mmwsig=" + mmwsig + ", mmwlnk=" + mmwlnk + ", mmwqry="
				+ mmwqry + ", mmtsno=" + mmtsno + ", mmtype=" + mmtype + "]";
	}
}
