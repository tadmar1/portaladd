package com.tadmar.portal.PortalAdd.controller;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.tadmar.portal.PortalAdd.data.shared.EnvirRepo;
import com.tadmar.portal.PortalAdd.model.ADInfoExt;
import com.tadmar.portal.PortalAdd.model.MobileMenu;
import com.tadmar.portal.PortalAdd.model.shared.CusrDa;
import com.tadmar.portal.PortalAdd.util.Utils;

@Named(value="HeaderCtrl")
@ViewScoped
public class HeaderCtrl implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private CusrDa cusrda;
	private int taskNo;
	private String headerText;
	@Inject private EnvirRepo envirRepo;
	
	private ADInfoExt aDinfoExt;
	/********************************************************************************************************
	 *  Constructors
	 ********************************************************************************************************/
	@PostConstruct
	private void init() {
		taskNo = 0;
		cusrda = envirRepo.findById(Utils.getUserName());
		onRetrieveUserInfo();
		initTitlesAndLabels();
	}

	/********************************************************************************************************
	 *  Events
	 ********************************************************************************************************/
	public void onRetrieveUserInfo() {
		aDinfoExt = RetrieveLdapUserInfo(cusrda.getCusrid());
	}
	public void onRefreshTaskData() {
		if (cusrda.getCusrid() != null) {
			List<MobileMenu> mobileMenuList = envirRepo.findMenuForDesktop("%");
			taskNo = 0;
			for (MobileMenu mobileMenu : mobileMenuList) {
				taskNo += envirRepo.findTasksNo(mobileMenu.getMmwqry().replace("{user}", cusrda.getCusrid().toUpperCase().trim()));
			}
		}
	}
	/********************************************************************************************************
	 *  Methods
	 ********************************************************************************************************/
	private ADInfoExt RetrieveLdapUserInfo(String userSgid) {
		
		ADInfoExt adinfoExt = new ADInfoExt();
		adinfoExt.setUserSgid(userSgid);
		adinfoExt.setClientIP(org.omnifaces.util.Faces.getRemoteAddr());
		return adinfoExt;
	}
	
	private void initTitlesAndLabels() {
		Map<String, String> requestMap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		if ( requestMap.get("label") != null ) {
			headerText = "  " + requestMap.get("label");
		} else {
			headerText = "  Baza Informacji Tadmaru";
		} 
	}
	/********************************************************************************************************
	 *  Get-Set
	 ********************************************************************************************************/
	public CusrDa getCusrda() {
		return cusrda;
	}
	public void setCusrda(CusrDa cusrda) {
		this.cusrda = cusrda;
	}
	public EnvirRepo getEnvirRepo() {
		return envirRepo;
	}
	public void setEnvirRepo(EnvirRepo envirRepo) {
		this.envirRepo = envirRepo;
	}
	public ADInfoExt getaDinfoExt() {
		return aDinfoExt;
	}
	public void setaDinfoExt(ADInfoExt aDinfoExt) {
		this.aDinfoExt = aDinfoExt;
	}

	public String getHeaderText() {
		return headerText;
	}

	public void setHeaderText(String headerText) {
		this.headerText = headerText;
	}

	public int getTaskNo() {
		return taskNo;
	}

	public void setTaskNo(int taskNo) {
		this.taskNo = taskNo;
	}
}
