package com.tadmar.portal.PortalAdd.service;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.DirContext;
import javax.naming.directory.ModificationItem;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.LdapContext;

import com.tadmar.portal.PortalAdd.model.ADInfo;

@Named(value="ADSearch")
@ViewScoped
public class ADSearch implements Serializable{

	private static final long serialVersionUID = 1L;
	private static final String IMG_BASE64_PREFIX="data:image/jpeg;base64,";

	@Inject private ADPersonGlobalServ aDPersonGlobalServ;
	
	@PostConstruct
	private void init() {}
	
	/********************************************************************************************************
	 *  Methods
	 ********************************************************************************************************/

	public void ModifyThumbnailPhoto(String dsn, byte[] newValue) {
		
		LdapContext ctx = aDPersonGlobalServ.getLdapContext();
		ModificationItem[] items = new ModificationItem[1];
		items[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
				new BasicAttribute("thumbnailPhoto", newValue));
		try {
	            ctx.modifyAttributes(dsn, items);
	            ctx.close();
	     } catch (NamingException e) {
	            e.printStackTrace();
	     }
	
	}
	public void ModifyInfo(String dsn, ADInfo adinfo) {

		LdapContext ctx = aDPersonGlobalServ.getLdapContext();
		ModificationItem[] items = new ModificationItem[1];

		try {
			try {
				items[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
						new BasicAttribute("physicalDeliveryOfficeName", adinfo.getPhysicalDeliveryOfficeName()));
				ctx.modifyAttributes(dsn, items);
			} catch (Exception ex) {}	

			try {
			items[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
				new BasicAttribute("telephoneNumber", adinfo.getTelephoneNumber()));
			ctx.modifyAttributes(dsn, items);
			} catch (Exception ex) {}	

			try {
			items[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
				new BasicAttribute("wWWHomepage", adinfo.getwWWHomepage()));
			ctx.modifyAttributes(dsn, items);
			} catch (Exception ex) {}	

			try {
			items[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
				new BasicAttribute("streetAddress", adinfo.getStreetAddress()));
			ctx.modifyAttributes(dsn, items);
			} catch (Exception ex) {}	

			try {
			items[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
				new BasicAttribute("postOfficeBox", adinfo.getPostOfficeBox()));
			ctx.modifyAttributes(dsn, items);
			} catch (Exception ex) {}	

			try {
			items[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
				new BasicAttribute("L", adinfo.getL()));
			ctx.modifyAttributes(dsn, items);
			} catch (Exception ex) {}	

			try{
			items[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
			new BasicAttribute("postalCode", adinfo.getPostalCode()));
			ctx.modifyAttributes(dsn, items);
			} catch (Exception ex) {}	

			try {
			items[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
				new BasicAttribute("co", adinfo.getCo()));
			ctx.modifyAttributes(dsn, items);
			} catch (Exception ex) {}	

			try {
			items[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
				new BasicAttribute("otherTelephone", adinfo.getOtherTelephone()));
			ctx.modifyAttributes(dsn, items);
			} catch (Exception ex) {}	

			try {
			items[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
				new BasicAttribute("pager", adinfo.getPager()));
			ctx.modifyAttributes(dsn, items);
			} catch (Exception ex) {}	

			try {
			items[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
				new BasicAttribute("mobile", adinfo.getMobile()));
			ctx.modifyAttributes(dsn, items);
			} catch (Exception ex) {}	

			try {
			items[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
				new BasicAttribute("facsimileTelephoneNumber", adinfo.getFacsimileTelephoneNumber()));
			ctx.modifyAttributes(dsn, items);
			} catch (Exception ex) {}	

			try {
			items[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
				new BasicAttribute("telephoneAssistant", adinfo.getTelephoneAssistant()));
			ctx.modifyAttributes(dsn, items);
			} catch (Exception ex) {}	

			try {
			items[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
				new BasicAttribute("description", adinfo.getDescription()));
			ctx.modifyAttributes(dsn, items);
			} catch (Exception ex) {}	

			try {
			items[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
				new BasicAttribute("title", adinfo.getTitle()));
			ctx.modifyAttributes(dsn, items);
			} catch (Exception ex) {}	

			if (adinfo.getNotRetrievedManager() != null && adinfo.getNotRetrievedManager().length() > 0) {
				try {
					items[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
							new BasicAttribute("manager", adinfo.getNotRetrievedManager()));
					ctx.modifyAttributes(dsn, items);
				} catch (Exception ex) {}
			}	

			ctx.close();
		} catch (NamingException e) {
			System.out.println(e.getMessage());
		}
	}
	
	public void modifyAttribute(String dsn, LdapContext ctx, String attribute, String  newValue) {
		ModificationItem[] items = new ModificationItem[1];
		items[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
				new BasicAttribute(attribute, newValue));
		try {
	            ctx.modifyAttributes(dsn, items);
	     } catch (NamingException e) {
	            e.printStackTrace();
	     }
	}

	public ADInfo getUserBasicAttributes(String username, LdapContext ctx, String[] attrIDs, String excludeAttr) {
		
		ADInfo adinfo = new ADInfo();
		Class<ADInfo> ADInfoClass = ADInfo.class;
		Field[] fields = ADInfoClass.getDeclaredFields();
		
		try {
			SearchControls constraints = new SearchControls();
			constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
			constraints.setReturningAttributes(attrIDs);
			NamingEnumeration<SearchResult> answer = ctx.search("DC=tdm,DC=local", "sAMAccountName="
                + username, constraints);
			if (answer.hasMore()) {
				Attributes attrs = ((SearchResult) answer.next()).getAttributes();
				for(int i =0; i < fields.length; i++ ){
					if (!fields[i].getName().startsWith("notRetrieved") && !fields[i].getName().equals(excludeAttr)) {
						if ( !fields[i].getName().equals("thumbnailPhoto") ) {
							try {
								if (fields[i].getName() == null) {
									fields[i].set(adinfo,"");
								} else if ( fields[i].getName().startsWith("manager")) {
									fields[i].set(adinfo,attrs.get(fields[i].getName())
											.get(0).toString().replace("\\,", " ").split(",")[0]
											.replace("CN=", ""));
								} else if ( fields[i].getName().startsWith("directReports")) {
									String tmpField = "";
									int numLines = attrs.get(fields[i].getName()).size();
									for ( int z = 0; z < numLines; z++) {
										if ( z > 0 ) {
											tmpField += ", ";
										}
										if (attrs.get(fields[i].getName()).get(z) != null) {
											tmpField += attrs.get(fields[i].getName()).get(z).toString()
													.replace("\\,", " ").split(",")[0].replace("CN=", "");
										}	
									}
									fields[i].set(adinfo, tmpField);
								} else {
									if ( attrs.get(fields[i].getName()).get(0) != null) {
										fields[i].set(adinfo,attrs.get(fields[i].getName()).get(0).toString());
									}
								}
							} catch(NullPointerException npe) {
								fields[i].set(adinfo,"");
							}
						} else {
							try {
								Object photo = attrs.get(fields[i].getName()).get(0);
								byte[] buf = (byte[])photo;
								fields[i].set(adinfo,buf);
								String base64encodedString = Base64.getEncoder().encodeToString(buf);
								adinfo.notRetrieved = IMG_BASE64_PREFIX + base64encodedString;
							} catch (NullPointerException npe) {}
						}
					}
				}
			}else{
				adinfo = new ADInfo();
			}
		} catch (Exception ex) {
			adinfo = new ADInfo();
		}
		return adinfo;
	}

	public String getFirstUserIfNoExist(String sgid) {
		return "";
	}
	
	public String getDepartmentName(ADInfo adinfo) {
		
		return adinfo.getDistinguishedName() != null ? adinfo.getDistinguishedName().replace("\\,", "").split(",")[1].replace("OU=", "") : "";
	}
	
	public List<ADInfo> getPersonOfDepartment(ADInfo adinfo, LdapContext ctx, String[] attrIDs) {
		
		List<ADInfo> adinfoList = new ArrayList<ADInfo>();
		if ( adinfo.getDistinguishedName() == null) return adinfoList;
		
		String[] departmentQueryMap = adinfo.getDistinguishedName().replace("\\,", "###")
				.replace("\\,", "").split(",");
		String departmentQuery = "";
		for ( int i = 1; i < departmentQueryMap.length; i++) {
			if ( i > 1) {
				departmentQuery += ",";
			}
			departmentQuery += departmentQueryMap[i];
		}
		departmentQuery = departmentQuery.replace("###", "\\,");
		
		try {
			SearchControls constraints = new SearchControls();
			constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
			constraints.setReturningAttributes(attrIDs);
			NamingEnumeration<SearchResult> answer = ctx.search(departmentQuery,"(objectclass=person)", constraints);
			while (answer.hasMore()) {
				Attributes attrs = ((SearchResult) answer.next()).getAttributes();
				String sgid = attrs.get("sAMAccountName").get(0).toString();
				adinfo = getUserBasicAttributes(sgid, ctx, getUserAttrIDs(),"");
				adinfoList.add(adinfo);
			}
		} catch(Exception ex) {	}
	
		return adinfoList;
	}
	
	@SuppressWarnings("rawtypes")
	public String[] getUserAttrIDs() {
		
		Class ADInfoClass = ADInfo.class;
		Field[] fields = ADInfoClass.getDeclaredFields();
		String[] fieldsNames = new String[fields.length];
		for ( int i = 0; i < fields.length; i++) {
			fieldsNames[i] = fields[i].getName();
		}	
		return fieldsNames;
	}
}
